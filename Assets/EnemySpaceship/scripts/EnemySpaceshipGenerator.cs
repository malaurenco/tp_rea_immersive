using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class EnemySpaceshipGenerator : MonoBehaviour
{
    public EnemySpaceship _enemySpaceship;
    private Transform _generatorTransform;
    
    public GameObject _target;
    private Transform _targetTransform;

    private List<EnemySpaceship> _listOfEnemies;
    private int _currentNumberOfSpaceship = 0;
    public int _maxEnemies = 10;

    public float _destroyDelay = 1.0f;
    public float _createDelay = 1.0f;

    public float _distanceFromMainSpaceship = 1000.0f;
    
    public Transform _tfCompas;//for compas
    public GameObject _arrow;//for compas
    public GameObject _currArrow;//for compas
    public List<GameObject> _arrowList;

    public int GetNumberOfEnemies()
    {
        return _currentNumberOfSpaceship;
    }
    
    public void CreateAnEnemySpaceship()
    {
        if (_currentNumberOfSpaceship < _maxEnemies) // verify to create an ennemy spaceship
        {
            EnemySpaceship es = Instantiate(_enemySpaceship, _generatorTransform.position, _generatorTransform.rotation, _generatorTransform.parent);
        
            es.Init(_target, _distanceFromMainSpaceship);

            _listOfEnemies.Add(es);
            _currentNumberOfSpaceship++;
            
            _currArrow = Instantiate(_arrow, _tfCompas);//for compas
            _currArrow.GetComponent<Arrow>()._target = es.GetComponent<Transform>();//for compas
            _currArrow.GetComponent<Arrow>().SetColor(new Color32(0xFF, 0x00, 0x00, 0));//for compas
            _arrowList.Add(_currArrow);//for compas
            
        }
    }

    public void DestroyEnemySpaceship()
    {
        int i = 0;//for compas
        foreach (EnemySpaceship es in _listOfEnemies.ToList())
        {
            if (es.isDead()) //Destroy l'arrow correspondant
            {
                _listOfEnemies.Remove(es);
                es.IDestroy();
                GameObject arr = _arrowList[i];
                _arrowList.Remove(arr);
                Destroy(arr);
                
                _currentNumberOfSpaceship--;

                Debug.Log("Ennemi abbatue !");
            }
            i++;//for compas
        }
    }

    IEnumerator SpaceshipCreator()
    {
        while (true)
        {
            yield return new WaitForSeconds(_createDelay);

            //Debug.Log("Coroutine: SpaceshipCreator treatment");
            CreateAnEnemySpaceship();
        }
    }

    IEnumerator SpaceshipDestructor()
    {
        while (true)
        {
            yield return new WaitForSeconds(_destroyDelay);

            //Debug.Log("Coroutine: SpaceshipDestructor treatment");
            DestroyEnemySpaceship();
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        _generatorTransform = GetComponent<Transform>();
        _listOfEnemies = new List<EnemySpaceship>(_maxEnemies);
        _targetTransform = _target.GetComponent<Transform>();
        /*
        _arrowList = new List<GameObject>();//for compas
        */
        StartCoroutine(SpaceshipCreator());
        StartCoroutine(SpaceshipDestructor());

        //CreateAnEnemySpaceship();
    }

    // Update is called once per frame
    void Update()
    {
    }
}
