using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SpaceshipTarget : MonoBehaviour, IDestructable
{
    
    public int _life;
    public AudioSource _hitSE;
    public RectTransform _lifeBar;
    public GameObject _lifeTextGo;
    private TextMeshProUGUI _lifeText;
    Vector2 _lifePos;

    public void IDestroy()
    {
        Debug.Log("Fin de la partie");
        SceneManager.LoadScene("StartScene");
    }

    public void IHit(float damage)
    {
        if (_hitSE != null) _hitSE.Play();
        _life -= (int) damage;
        _lifePos.x = (_life * 10) / 2;
        _lifeBar.anchoredPosition = _lifePos;
        _lifeBar.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, _life * 10);
        _lifeText.text = "Life : " + _life + "/" + 10;
        if (_life < 1) IDestroy();
    }

    public bool isDead()
    {
        return _life <= 0;
    }

    // Start is called before the first frame update
    void Start()
    {
        _life = 10;
        _lifeText = _lifeTextGo.GetComponent<TextMeshProUGUI>();
        _lifePos = new Vector2(0, 5);
        _lifePos.x = (_life * 10) / 2;
        _lifeBar.anchoredPosition = _lifePos;
        _lifeBar.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, _life * 10);
        _lifeText.text = "Life : " + _life + "/" + 10;
    }

    // Update is called once per framef
    void Update()
    {
        
    }
}
