using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.WSA;
using static UnityEngine.GraphicsBuffer;


public class LaserCollider : MonoBehaviour
{
    private GameObject _source;
    private GameObject _target;

    protected Transform _tf;
    private Transform _sourceTf;
    private Transform _targetTf;

    public GameObject _launcher;

    private Vector3 _targetPosition;
    private Vector3 _randomTargetDirection;

    public float _speed = 500f;
    public float _maxDistance = 500f;
    private float _damage;

    private bool _enabled = false;

    // Audio members
    public AudioSource _launchSE = null;

    public virtual void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject != _launcher)
        {
            var tempMonoArray = collision.gameObject.GetComponents<MonoBehaviour>();

            foreach (var monoBehaviour in tempMonoArray)
            {
                if (monoBehaviour is IDestructable tempCollidable)
                {
                    //Debug.Log("Laser en collision avec: " + collision.gameObject.name);
                    //tempCollidable.IDestroy();
                    tempCollidable.IHit(_damage);

                    Destroy(gameObject);
                    //StartCoroutine(DelayedDestroy());

                    break;
                }
            }
        }
    }

    public IEnumerator DelayedDestroy()
    {
        gameObject.transform.localScale = new Vector3(0, 0, 0);
        yield return new WaitForSeconds(2f);
        Destroy(gameObject);
    }

    public void UpdateState()
    {
        //_targetPosition = new Vector3(_targetTf.position.x, _targetTf.position.y, _targetTf.position.z);
        //_targetPosition += _randomTargetDirection;

        if (_sourceTf != null && Vector3.Distance(_tf.position, _sourceTf.position) > _maxDistance)
        {
            //Debug.Log("Cible NON touch�e");
            Destroy(gameObject);
        }
    }

    public virtual void UpdatePosition()
    {
        float step = _speed * Time.deltaTime;
        _tf.Translate(0, step, 0);

        //_tf.position = Vector3.MoveTowards(_tf.position, _targetPosition, step);
    }

    public void Init(GameObject parent, GameObject launcher, float damage, bool inaccurate = false)
    {
        _source = GameObject.Find(parent.name);
        _target = null;
        _launcher = launcher;
        _damage = damage;

        _tf = GetComponent<Transform>();
        _source.TryGetComponent<Transform>(out _sourceTf);
        _targetTf = _sourceTf;
        _targetPosition = new Vector3(_targetTf.position.x, _targetTf.position.y, _targetTf.position.z);

        if (inaccurate == true)
        {
            _randomTargetDirection = new Vector3(UnityEngine.Random.Range(-50, 50),
                                                 UnityEngine.Random.Range(-10, 10),
                                                 UnityEngine.Random.Range(-50, 50));
            _targetPosition += _randomTargetDirection;
        }
        else
        {
            _randomTargetDirection = Vector3.zero;
        }

        _tf.LookAt(_targetPosition);
        _tf.Rotate(new Vector3(90, 0, 0));

        if (_launchSE != null) _launchSE.Play();


        _enabled = true;
    }

    public void Init(GameObject parent, GameObject target, GameObject launcher, float damage, bool inaccurate = false)
    {
        _tf = GetComponent<Transform>();

        _source = GameObject.Find(parent.name);
        _target = GameObject.Find(target.name);
        _launcher = launcher;

        _damage = damage;
        
        _source.TryGetComponent<Transform>(out _sourceTf);
        _targetTf = _target.GetComponent<Transform>();

        _maxDistance = 1.2f * Vector3.Distance(_tf.position, _targetTf.position);

        _targetPosition = new Vector3(_targetTf.position.x, _targetTf.position.y, _targetTf.position.z);

        if (inaccurate == true)
        {
            _randomTargetDirection = new Vector3(UnityEngine.Random.Range(-50, 50), 
                                                 UnityEngine.Random.Range(-10, 10), 
                                                 UnityEngine.Random.Range(-50, 50));
            _targetPosition += _randomTargetDirection;
        }
        else
        {
            _randomTargetDirection = Vector3.zero;
        }

        _tf.LookAt(_targetPosition);
        _tf.Rotate(new Vector3(90, 0, 0));

        if (_launchSE != null) _launchSE.Play();

        _enabled = true;
    }

    private void CheckSourceTf()
    {
        if (_sourceTf == null)
        {
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (_enabled)
        {
            CheckSourceTf();
            UpdateState();
            UpdatePosition();
        }
    }
}
