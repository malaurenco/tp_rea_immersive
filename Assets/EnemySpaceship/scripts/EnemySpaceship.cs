using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class EnemySpaceship : MonoBehaviour, IDestructable
{
    public GameObject _target;

    public AudioSource _hitSE;

    private Transform _targetTransform;
    private Transform _tf;
    private Transform _tfForRotation;

    public LaserCollider _laserPrefab = null;
    private LaserCollider _laserbeam;

    public int _life = 10;
    public int _damage = 1;

    public int _probaAttack = 1; // pourcentage de chance d'attaquer

    public float _moveSpeed = 100f;
    public float _rotationSpeed = 100f;
    private float _moveXDirection;

    public float _attackDelay = 5f;

    public float _distanceFromMainSpaceship = 1000.0f;

    public bool isDead()
    {
        return (_life <= 0);
    }

    public void Move(Vector3 direction)
    {
        // move the spaceship to a direction
    }

    IEnumerator Attack()
    {
        while (true)
        {
            if (_laserbeam == null)
            {
                _laserbeam = Instantiate(_laserPrefab, _tf.position, _tf.rotation, _tf.parent);
                _laserbeam.Init(gameObject, _target, gameObject, _damage, true);

                yield return new WaitForSeconds(_attackDelay);
            }
            yield return null;
        }
    }

    public void InitRandomDirections()
    {
        int rand = Random.Range(0, 100);
        if (rand < 33) _moveXDirection = -1;
        else if (rand < 66) _moveXDirection = 1;
        else _moveXDirection = 0;

        _moveSpeed = Random.Range(0.8f * _moveSpeed, 1.2f * _moveSpeed);
    }

    public void IDestroy()
    {
        StartCoroutine(DelayedDestroy());
        //Destroy(gameObject);
    }

    public IEnumerator DelayedDestroy()
    {
        gameObject.transform.localScale = new Vector3(0, 0, 0);
        yield return new WaitForSeconds(1f);
        Destroy(gameObject);
    }

    public void IHit(float damage)
    {
        if (_hitSE != null) _hitSE.Play();
        _life -= (int) damage;
    }

    public void Init(GameObject target, float distanceFromMainSpaceship)
    {
        // Init the spaceship and appear in the scene
        _tf = GetComponent<Transform>();
        _tfForRotation = _tf.GetChild(0);

        _target = GameObject.Find(target.name);
        _target.TryGetComponent<Transform>(out _targetTransform);

        _distanceFromMainSpaceship = distanceFromMainSpaceship + Random.Range(0, 100);
        _tf.Translate(Random.Range(-200, 200), Random.Range(0, 200), -_distanceFromMainSpaceship);
        _tf.LookAt(_targetTransform);

        _rotationSpeed = Random.Range(0.8f * _rotationSpeed, 1.2f * _rotationSpeed);

        InitRandomDirections();
        StartCoroutine(Attack());
    }

    public void Move()
    {
        // move the spaceship
        _tf.LookAt(_targetTransform);
        _tf.Translate(_moveXDirection * _moveSpeed * Time.deltaTime, 0, 0);

        float error = 50f;
        float step = 1f;
        float distance = Vector3.Distance(_tf.position, _targetTransform.position);

        if (distance > _distanceFromMainSpaceship + error)
        {
            _tf.Translate(0, 0, step);
        }
        else if (distance < _distanceFromMainSpaceship - error)
        {
            _tf.Translate(0, 0, -step);
        }
    }

    private void SpaceshipAnimation()
    {
        _tfForRotation.Rotate(0, _rotationSpeed * Time.deltaTime, 0);
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Move();
        SpaceshipAnimation();
    }
}
