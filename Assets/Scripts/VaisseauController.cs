using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VaisseauController : MonoBehaviour
{
    Transform _tf;
    float _rotation1;
    float _rotation2;
    Vector3 _rot;
    //bool _moving;
    Transform _tfSpaceship;

    // Start is called before the first frame update
    void Start()
    {
        _tf = GetComponent<Transform>();
        _tfSpaceship = GameObject.Find("spaceShip").GetComponent<Transform>();
        _rot = new Vector3();

        //_persoTf.SetParent(_tf);
    }

    // Update is called once per frame
    void Update()
    {
        _tf.Rotate(_rot);
    }

    /// <summary>
    /// Gets the X value of the joystick. Called by the <c>XRJoystick.OnValueChangeX</c> event.
    /// </summary>
    /// <param name="x">The joystick's X value</param>
    public void OnJoystickValueChangeX(float x)
    {
        _rot.y = x;
    }

    /// <summary>
    /// Gets the Y value of the joystick. Called by the <c>XRJoystick.OnValueChangeY</c> event.
    /// </summary>
    /// <param name="y">The joystick's Y value</param>
    public void OnJoystickValueChangeY(float y)
    {
        _rot.x = y;
    }
}
