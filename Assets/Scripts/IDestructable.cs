using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDestructable
{
    public void IDestroy();

    public void IHit(float damage);

    public bool isDead();
}
