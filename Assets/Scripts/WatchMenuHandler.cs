using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace UnityEngine.XR.Content.Interaction
{

    public class WatchMenuHandler : MonoBehaviour
    {
        LocomotionManager _locManager;
        public Slider _soundSlider;
        public GameObject _snapSlider;
        public GameObject _continuousSlider;

        // Start is called before the first frame update
        void Start()
        {
            _locManager = GetComponent<LocomotionManager>();
            _soundSlider.normalizedValue = AudioListener.volume;
        }

        // Update is called once per frame
        void Update()
        {

        }

        public void toggleSnapTurn(bool snap)
        {
            _locManager.rightHandTurnStyle = snap ? LocomotionManager.TurnStyle.Snap : LocomotionManager.TurnStyle.Smooth;
            _continuousSlider.SetActive(!snap);
            _snapSlider.SetActive(snap);
        }

        public void toggleComfortMode(bool comfort)
        {
            _locManager.enableComfortMode = comfort;
        }

        public void adjustSnapAmount(float amount)
        {
            _locManager.snapTurnProvider.turnAmount = amount;
        }

        public void adjustContinuousSpeed(float speed)
        {
            _locManager.smoothTurnProvider.turnSpeed = speed;
        }

        public void QuitGame()
        {
            SceneManager.LoadScene("StartScene");
            _soundSlider.normalizedValue = AudioListener.volume;
        }

        public void RestartGame()
        {
            SceneManager.LoadScene("Main_scene");
            _soundSlider.normalizedValue = AudioListener.volume;
        }

        public void AdjustVolume(float volume)
        {
            AudioListener.volume = volume;
        }
    }
}