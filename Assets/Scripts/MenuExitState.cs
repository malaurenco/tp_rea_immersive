using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuExitState : StateMachineBehaviour
{
    public GameObject _mainMenu;
    public GameObject _settingsMenu;

    // Start is called before the first frame update
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.gameObject.transform.GetChild(0).gameObject.SetActive(true);
        animator.gameObject.transform.GetChild(1).gameObject.SetActive(false);
    }
}