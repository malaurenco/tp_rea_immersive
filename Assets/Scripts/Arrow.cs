using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour
{
    [Tooltip ("The object transform to target")]
    public Transform _target;
    void Start()
    {
    }

    public void SetColor(Color color)
    {
        transform.GetChild(0).GetComponent<Renderer>().material.color = color;
        transform.GetChild(1).GetComponent<Renderer>().material.color = color;
    }

    
    void Update()
    {
        transform.LookAt(_target);
    }
}
