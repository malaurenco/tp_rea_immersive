using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class Asteroide : MonoBehaviour
{
    public float _distDisparition; // same as AsteroidGenerator _distApparition
    public Rigidbody _myRb;
    public Transform _tfAsteroidGenerator;
    public Transform _myTf;
    public float _vitesse;
    public AsteroideGenerator _asteroideGenerator;
    public float _baseScale;
    public float _scale1;
    public float _scale2;
    public float _scale3;
    public int _minableValue;
    public int _minableValueInit;
    private Vector3 _scale;
    //
    Mesh mesh;
    //MeshCollider meshCol;
    public Vector3[] vertices;
    public List<int> PtsInd;
    public List<float> Strength;


    public GameObject _arrow;
    
    //
    // Start is called before the first frame update
    void Start()
    {
        _myRb = GetComponent<Rigidbody>();
        _myTf = GetComponent<Transform>();
        //_tfAsteroidGenerator = _myTf.parent.GetComponent<Transform>();
        _scale1 = UnityEngine.Random.Range(0.75f, 1.5f);
        _scale2 = UnityEngine.Random.Range(0.75f, 1.5f);
        _scale3 = UnityEngine.Random.Range(0.75f, 1.5f);
        //_asteroideGenerator = _myTf.parent.GetComponent<AsteroideGenerator>();
        //
        Strength = new List<float>();
        PtsInd = new List<int>();
        mesh = GetComponent<MeshFilter>().mesh;
        //meshCol = GetComponent<MeshCollider>();
        vertices = new Vector3[mesh.vertices.Length];
        Array.Copy(mesh.vertices, vertices, mesh.vertices.Length);
        GenPts();
        Deform();
        mesh.vertices = vertices;
        //meshCol.sharedMesh = mesh;
        //

        _scale = new Vector3(_scale1, _scale2, _scale3);
        //_myTf.localScale = Vector3.Scale(_myTf.localScale , scale * _baseScale);
        _myTf.localScale = _scale * _baseScale*2;
        if (_baseScale > 10)
        {
            GetComponent<Renderer>().material.color = new Color32(0xFF, 0xD7, 0x00, 0);
        }
        else
        {
            GetComponent<Renderer>().material.color = Color.black;
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (Vector3.Distance(_tfAsteroidGenerator.position, _myTf.position) > _distDisparition)
        {
            _asteroideGenerator.Destructed((_baseScale>10)?1:0);
            if (_arrow) Destroy(_arrow);
            Destroy(gameObject);
        }
    }

    void Deform()
    {
        Vector3 Point;
        Vector3 ptToVertex;
        Vector3 max;
        for (int j = 0; j < PtsInd.Count; j++)
        {
            Point = vertices[PtsInd[j]] * 1.1f;
            for (int i = 0; i < vertices.Length; i++)
            {
                ptToVertex = Point - vertices[i];
                vertices[i] -= Strength[j] / (1 + ptToVertex.sqrMagnitude) * ptToVertex.normalized;

            }
        }
        max = new Vector3(0,0,0);
        for (int i=0; i<vertices.Length;i++){
            if (vertices[i].magnitude>max.magnitude){
                max = vertices[i];
            }
        }
        for (int i=0; i<vertices.Length;i++){
            vertices[i]/=max.magnitude*2;
        }

    }
    public void GenPts()
    {
        float x = UnityEngine.Random.Range(15, 25);
        Strength.Clear();
        PtsInd.Clear();
        for (int i = 0; i < x; i++)
        {
            PtsInd.Add((int)UnityEngine.Random.Range(0, vertices.Length));
            Strength.Add(UnityEngine.Random.Range(0.1f, 0.5f));
        }
    }

    public void setMinableVal(int v)
    {
        _minableValue = v;
        _minableValueInit = v;
    }

    public bool mined()
    {
        _minableValue--;
        if (_minableValue < 1)
        {
            _asteroideGenerator.Destructed((_baseScale > 10) ? 1 : 0);
            if (_arrow) Destroy(_arrow);
            Destroy(gameObject);
            return true;
        }
        _myTf.localScale = _scale * _baseScale * 2.0f * (float)(_minableValue)/_minableValueInit;
        return false;
    }
}
