using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class DetectEnemy : MonoBehaviour
{
    private List<EnemySpaceship> _enemy;

    private void OnTriggerEnter(Collider other)
    {
        EnemySpaceship es = other.GetComponent<EnemySpaceship>();
        if (es != null)
        {
            Debug.Log("Ennemi per�u");
            _enemy.Add(es);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        EnemySpaceship es = other.GetComponent<EnemySpaceship>();
        if (es != null)
        {
            Debug.Log("Ennemi disparu");
            RemoveEnemy(es);
        }
    }

    public EnemySpaceship GetEnemy()
    {
        if (_enemy.Count > 0)
        {
            return _enemy.First();
        }
        return null;
    }

    public void RemoveEnemy()
    {
        if (_enemy.Count > 0)
        {
            _enemy.Remove(_enemy.First());
        }
    }

    public void RemoveEnemy(EnemySpaceship enemy)
    {
        if (_enemy.Count > 0)
        {
            _enemy.Remove(enemy);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        _enemy = new List<EnemySpaceship>();
    }

    // Update is called once per frame
    void Update()
    {
        _enemy.RemoveAll(x => !x);
    }
}
