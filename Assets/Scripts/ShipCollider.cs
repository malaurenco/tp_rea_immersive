using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipCollider : MonoBehaviour
{
    Vector3 tran;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == 6)
        {
            Debug.Log("asteroid In");
            //other.transform.Translate(1 * other.impulse / Time.fixedDeltaTime);
            //other.rigidbody.AddForce(10 * other.impulse / Time.fixedDeltaTime);
            tran = other.transform.position - transform.position;
            other.GetComponent<Rigidbody>().AddForce(100*(tran));
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == 6)
        {
            Debug.Log("asteroid In");
            //other.transform.Translate(1 * other.impulse / Time.fixedDeltaTime);
            //other.rigidbody.AddForce(10 * other.impulse / Time.fixedDeltaTime);
            other.GetComponent<Rigidbody>().AddForce(-90 * (tran));
        }
    }
    /*
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.layer == 6)
        {
            //Debug.Log("asteroid In");
            //other.rigidbody.AddForce(2 * other.impulse / Time.fixedDeltaTime);
            other.GetComponent<Rigidbody>().AddForce(2 * (other.transform.position - transform.position));
        }
    }*/
}
