using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VaisseauControllerTr : MonoBehaviour
{
    private Transform _tf;
    private Transform _spaceship;

    private float _speed = 100f;

    float _translationForward;
    float _translationUp;
    float _translationRight;

    // Start is called before the first frame update
    void Start()
    {
        _tf = GetComponent<Transform>();
        _spaceship = GameObject.Find("spaceShip").GetComponent<Transform>();

        _translationForward = 0;
        _translationUp = 0;
        _translationRight = 0;

        //_persoTf.SetParent(_tf);
    }

    // Update is called once per frame
    void Update()
    {
        _tf.Translate(Vector3.forward * (_translationForward * Time.deltaTime * _speed), _spaceship);
        _tf.Translate(Vector3.up * (_translationUp * Time.deltaTime * _speed), _spaceship);
        _tf.Translate(Vector3.right * (_translationRight * Time.deltaTime * _speed), _spaceship);
    }


    /// <summary>
    /// Gets the Y value of the joystick. Called by the <c>XRJoystick.OnValueChangeY</c> event.
    /// </summary>
    /// <param name="y">The joystick's Y value</param>
    public void OnRightJoystickValueChangeY(float y)
    {
        _translationUp = y;
    }

    public void OnLeftJoystickValueChangeY(float y)
    {
        _translationForward = -y;
    }

    public void OnLeftJoystickValueChangeX(float x)
    {
        _translationRight = -x;
    }
}
