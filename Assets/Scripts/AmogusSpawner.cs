using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmogusSpawner : MonoBehaviour
{
    public GameObject _amogusPrefab;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SpawnCrewmate()
    {
        Instantiate(_amogusPrefab, transform);
    }
}
