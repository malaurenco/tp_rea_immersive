using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WatchMenuScript : MonoBehaviour
{
    public GameObject _menuCanvas;
    public Transform _headTf;
    public Animator anim;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 vectFromMain = (transform.up - transform.right).normalized;
        Vector3 mainToHead = (_headTf.position - transform.position).normalized;
        /*if(Vector3.Dot(vectFromMain, mainToHead) > 0.9 && Vector3.Dot(transform.forward, _headTf.right) > 0.9)
        {
            anim.Play("menu_open");
            //_menuCanvas.SetActive(true);
        }
        else
        {
            anim.Play("menu_close");
            //_menuCanvas.SetActive(false);
        }*/

        anim.SetBool("opened", Vector3.Dot(vectFromMain, mainToHead) > 0.8 && Vector3.Dot(transform.forward, _headTf.right) > 0.9);
    }
}
