using System.Collections.Generic;
using UnityEngine;

public class GoldGestion : MonoBehaviour
{
    public GameObject _goldPrefab;

    public GameObject _confirmMenu;
    public GameObject _menuMenu;
    public GameObject _welcomeMenu;
    private int _goldValue;

    public int debug;
    public int Nodebug;


    private float _sizeY = 0.144f;



    private float _goldSizeX = 0.25f;
    private float _goldSizeY = 0.04f;


    List<GameObject> _goldIngot = new List<GameObject>();



    // Start is called before the first frame update
    void Start()
    {
        _goldValue = 0;
        debug = 0;
    }

    // Update is called once per frame
    void Update()
    {
        for(; debug > 0; debug--)
        {
            AddGold();
        }

        for (; Nodebug > 0; Nodebug--)
        {
            RemoveGold();
        }
    }

    public bool TryRemoveGold(int val)
    {
        if(val <= _goldValue)
        {
            RemoveGolds(val);
            return true;
        }
        else
        {
            return false;
        }
    }

    private void RemoveGolds(int gv)
    {
        for(int i = 0; i < gv; i++)
        {
            RemoveGold();
        }
    }

    private void RemoveGold()
    {
        GameObject tmp = _goldIngot[_goldValue - 1];
        Destroy(tmp);
        _goldIngot.RemoveAt(_goldValue - 1);
        _goldValue--;
    }

    public void AddGolds(int nb)
    {
        for(int i = 0; i < nb; i++)
        {
            AddGold();
        }
    }

    public void AddGold()
    {
        int haut = (_goldValue/68);
        bool ispair = (_goldValue - haut * 68 > 35);
        int numberInCouche = _goldValue - haut * 68 - ((ispair) ? 36 : 0);
        float x = ((ispair) ? numberInCouche/4-3.5f: numberInCouche / 12 -1f);
        float y = ((ispair) ? numberInCouche % 4 - 1.6f : numberInCouche % 12-6f);
        haut = (haut * 2) + (ispair ? 1 : 0);


        GameObject justCreated = Instantiate(_goldPrefab, 
            GetComponent<Transform>().position 
            + GetComponent<Transform>().up*((_sizeY + _goldSizeY)/2 + haut*_goldSizeY)
            + GetComponent<Transform>().forward*(y*(((ispair)?_goldSizeX * 1.05f : (_goldSizeY * 2.2f) ) ) + (_goldSizeY / 4 * 3))
            + GetComponent<Transform>().right*(x* (((!ispair) ? _goldSizeX * 1.05f : _goldSizeY * 2.2f) )), (!ispair)?Quaternion.identity: Quaternion.Euler(0, 90, 0),
            transform);
        _goldIngot.Add(justCreated);
        _goldValue++;
    }

    /*private void comeBack()
    {
        _confirmMenu.SetActive(false);
        _welcomeMenu.SetActive(true);

    }*/
    public void onBuy()
    {
        _menuMenu.SetActive(false);
        _confirmMenu.SetActive(true);
        //Invoke("comeBack()", 10);
    }

    public int GetGold()
    {
        return _goldValue;
    }
}
