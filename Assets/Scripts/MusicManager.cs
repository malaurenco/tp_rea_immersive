using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour
{
    public AudioSource _spaceshipInterior = null;
    public AudioSource _spaceshipBattle = null;
    public AudioSource _enemy = null;

    public EnemySpaceshipGenerator _enSpGe = null;

    public Coroutine _fadeIn;
    public Coroutine _fadeOut;
    public Coroutine _check;

    public bool _battle = false;
    public bool _hasEnemy = false;

    private bool _isInteriorPlaying = false;
    private bool _isBattlePlaying = false;
    private bool _isEnemyPlaying = false;

    private float _sIVolume;
    private float _sBVolume;
    private float _eVolume;

    private float _checkTime = 2f;

    public void BeginBattle()
    {
        _battle = true;
    }

    public void EndBattle()
    {
        _battle = false;
    }

    public IEnumerator CheckEnnemi()
    {
        while (true)
        {
            if (_enSpGe.GetNumberOfEnemies() > 0)
            {
                _hasEnemy = true;
            }
            else
            {
                _hasEnemy = false;
            }
            yield return new WaitForSeconds(_checkTime);
        }
    }

    public void EnemyAppear()
    {
        _hasEnemy = true;
    }

    public void NoEnemy()
    {
        _hasEnemy= false;
    }

    // Start is called before the first frame update
    void Start()
    {
        _battle = false;

        _isInteriorPlaying = false;
        _isBattlePlaying = false;
        _isEnemyPlaying = false;

        _hasEnemy = false;

        _spaceshipInterior.loop = true;
        _spaceshipBattle.loop = true;
        _enemy.loop = true;

        _sIVolume = _spaceshipInterior.volume;
        _sBVolume = _spaceshipBattle.volume;
        _eVolume = _enemy.volume;

        _spaceshipInterior.volume = 0;
        _spaceshipBattle.volume = 0;
        _enemy.volume = 0;

        StartCoroutine(AudioFadeScript.FadeIn(_spaceshipInterior, 5f, _sIVolume));
        _isInteriorPlaying = true;

        if (_enSpGe != null) _check = StartCoroutine(CheckEnnemi());
    }

    void StopFadeCoroutines()
    {
        if (_fadeIn != null) StopCoroutine(_fadeIn);
        if (_fadeOut != null) StopCoroutine(_fadeOut);
    }

    void StopSound(AudioSource audio, ref bool _isPlaying)
    {
        audio.Stop();
        audio.volume = 0;
        _isPlaying = false;
    }

    void NoEnemyMusicSpaceship()
    {
        if (_battle == false && _isInteriorPlaying == false)
        {
            StopFadeCoroutines();
            StopSound(_enemy, ref _isEnemyPlaying);
            if (_isBattlePlaying == true)
            {
                _fadeOut = StartCoroutine(AudioFadeScript.FadeOut(_spaceshipBattle, 0.5f));
                _isBattlePlaying = false;
            }
            _fadeIn = StartCoroutine(AudioFadeScript.FadeIn(_spaceshipInterior, 5f, _sIVolume));
            _isInteriorPlaying = true;
        }
        else if (_battle == true && _isBattlePlaying == false)
        {
            StopFadeCoroutines();
            StopSound(_enemy, ref _isEnemyPlaying);
            if (_isInteriorPlaying == true)
            {
                _fadeOut = StartCoroutine(AudioFadeScript.FadeOut(_spaceshipInterior, 0.5f));
                _isInteriorPlaying = false;
            }
            _fadeIn = StartCoroutine(AudioFadeScript.FadeIn(_spaceshipBattle, 5f, _sBVolume));
            _isBattlePlaying = true;
        } else
        {
            // do nothing
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (_hasEnemy == true)
        {
            if (_isEnemyPlaying == false)
            {
                StopFadeCoroutines();
                _fadeIn = StartCoroutine(AudioFadeScript.FadeIn(_enemy, 2f, _eVolume));
                _isEnemyPlaying = true;
                StopSound(_spaceshipBattle, ref _isBattlePlaying);
                StopSound(_spaceshipInterior, ref _isInteriorPlaying);
            }
        }
        else
        {
            if (_isEnemyPlaying == true)
            {
                _fadeOut = StartCoroutine(AudioFadeScript.FadeOut(_enemy, 4f));
                _isEnemyPlaying = false;
            }
            NoEnemyMusicSpaceship();
        }
    }
}
