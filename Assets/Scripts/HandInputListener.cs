using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class HandInputListener : MonoBehaviour
{
    public InputActionProperty gripAction;
    public Animator handAnimator;

    // Update is called once per frame
    void Update()
    {
        float gripValue = gripAction.action.ReadValue<float>();
        handAnimator.SetFloat("Grip", gripValue);
    }
}
