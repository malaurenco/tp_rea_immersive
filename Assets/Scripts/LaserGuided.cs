using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LaserGuided : LaserCollider
{
    public DetectEnemy _detector;

    public override void UpdatePosition()
    {
        float step = _speed * Time.deltaTime;

        EnemySpaceship target = _detector.GetEnemy();

        if (target != null)
        {
            Vector3 targetPosition = target.transform.position;
            targetPosition.y += 10;

            _tf.LookAt(targetPosition);
            _tf.Rotate(new Vector3(90, 0, 0));
            _tf.position = Vector3.MoveTowards(_tf.position, targetPosition, step);
        }
        else
        {
            _tf.Translate(0, step, 0);
        }
    }

    void Start()
    {
        _tf = GetComponent<Transform>();
        _detector = GameObject.Find("ViseEnemy").GetComponent<DetectEnemy>();
        Debug.Log(_detector != null);
    }
}
