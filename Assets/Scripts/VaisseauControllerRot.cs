using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VaisseauControllerRot : MonoBehaviour
{
    private Transform _tf;
    
    //public Material _skb;

    //private float _rotationX;
    private float _rotationZ;

    //private float _rotatedX;
    //private float _rotatedZ;

    private float _speed = 50f;

    // Start is called before the first frame update
    void Start()
    {
        _tf = GetComponent<Transform>();

        //_rotatedX = 0f;
        //_rotationX = 0;
    }

    // Update is called once per frame
    void Update()
    {
        //float moveX = _rotationX * Time.deltaTime * _speed;
        float moveZ = _rotationZ * Time.deltaTime * _speed;

        //_rotatedX += moveX;
        //_rotatedZ += moveZ;

        //GameObject.Find("spaceShip").GetComponent<Transform>().Rotate(Vector3.up * -moveX, Space.World);
        //GameObject.Find("Complete XR Origin").GetComponent<Transform>().Rotate(Vector3.up * moveX, Space.World);
        _tf.Rotate(Vector3.up * moveZ, Space.World);
        //_tf.Rotate(Vector3.right * moveX, Space.World);

        //_skb.SetFloat("_UVSec", _rotated);

        //_skb.SetFloat("_Rotation", -_rotatedZ);
        //_skb.SetVector("_RotationAxis", new Vector4(0, 1, 0, 1));
        //_skb.SetFloat("_Rotation", -_rotatedX);
        //_skb.SetVector("_RotationAxis", new Vector4(1, 0, 0, 1));
        //_skb.SetFloat("_Rotation", -_rotatedZ);

        //Debug.Log(_rotationX + "  " + _rotatedX);
    }

    public void OnRightJoystickValueChangeX(float x)
    {
        _rotationZ = -x;
    }
}
