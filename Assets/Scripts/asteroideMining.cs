using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class asteroideMining : MonoBehaviour
{
    public float _tempsDeMinageDeLingot = 5.0f;
    public GameObject _objetRayon;
    public GameObject _goldStock;

    public GameObject _startLazer;

    public int nbUpgrade;

    private int level;
    private List<GameObject> _enteredAste;
    private List<GameObject> _rayons;
    private int _minabeAste = 0;
    private float _timeBeforeNext = -1.0f;
    // Start is called before the first frame update
    void Start()
    {
        nbUpgrade = 0;
        level = 0;
        _enteredAste = new List<GameObject>();
        _rayons = new List<GameObject>();
    }

    // Update is called once per frame
    void Update()
    {
        if (_minabeAste > 0)
        {
            if (_timeBeforeNext - Time.deltaTime < 0)
            {
                _timeBeforeNext = _tempsDeMinageDeLingot;
                //Debug.Log("minage+1  " + _minabeAste);
                int which = 0;
                if (_enteredAste[which].GetComponent<Asteroide>().mined()){
                    _minabeAste--;
                    _enteredAste.RemoveAt(which);
                    Destroy(_rayons[which]);
                    _rayons.RemoveAt(which);
                }
                _goldStock.GetComponent<GoldGestion>().AddGold();
            }
            _timeBeforeNext -= Time.deltaTime;

            for (int go = 0; go < _rayons.Count; go++)
            {
                _rayons[go].GetComponent<LineRenderer>().SetPosition(0, _startLazer.GetComponent<Transform>().position);
                _rayons[go].GetComponent<LineRenderer>().SetPosition(1, _enteredAste[go].GetComponent<Transform>().position);
            }
        }
        for(; nbUpgrade>0; nbUpgrade--)
        {
            upgrade();
        }
    }

    public void upgrade()
    {
        if(level%3 == 0)
        {
            GetComponent<Transform>().position = GetComponent<Transform>().position - GetComponent<Transform>().up * (float)((GetComponent<Transform>().localScale.z / 4.0) * 3.0925);
            GetComponent<Transform>().localScale *= 2;
        }else if(level%3 == 1)
        {
            _tempsDeMinageDeLingot = _tempsDeMinageDeLingot / 5.0f * 4.0f;
        }
        level++;
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Minable"))
        {
            if (_minabeAste == 0)
            {
                _timeBeforeNext = _tempsDeMinageDeLingot;
            }
            //Debug.Log("Un entre");
            _minabeAste++;
            _enteredAste.Add(other.gameObject);

            GameObject justCreated = Instantiate(_objetRayon, GetComponent<Transform>().position, Quaternion.identity);
            //justCreated.GetComponent<Renderer>().material.SetColor("_Color", Color.cyan);
            _rayons.Add(justCreated);

        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Minable"))
        {
            _minabeAste--;
            for (int go = 0; go < _enteredAste.Count; go++)
            {
                if (_enteredAste[go].Equals(other.gameObject))
                {
                    _enteredAste.RemoveAt(go);
                    Destroy(_rayons[go]);
                    _rayons.RemoveAt(go);


                    //Debug.Log("Un sort");
                }
            }
        }
    }
}