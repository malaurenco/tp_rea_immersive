using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.XR;

public class SpaceshipCanon : MonoBehaviour
{
    private GameObject _target;
    private Transform _tf;

    public LaserCollider _laserPrefab;
    public LaserGuided _laserGuidedPrefab;
    public GameObject _spaceshipTarget;

    public float _attackDelay = 0.5f;

    public float _basicDamage = 2.0f;
    public float _guidedDamage = 1.0f;

    public InputActionProperty _triggerAction;
    public Animator _handAnimator;

    public Transform _world;

    private bool _attacking = false;

    public int _numberOfGuidedLaser = 0;

    public void SetBasicDamage(float damage)
    {
        _basicDamage = damage;
    }

    public void SetGuidedDamage(float damage)
    {
        _guidedDamage = damage;
    }

    public float GetBasicDamage()
    {
        return _basicDamage;
    }

    public float GetGuidedDamage()
    {
        return _guidedDamage;
    }

    public void AddGuidedLaser(int n)
    {
        _numberOfGuidedLaser += n;
    }

    public void RemoveGuidedLaser(int n)
    {
        _numberOfGuidedLaser -= n;
    }

    public void StopAttacking()
    {
        _attacking = false;
        StopCoroutine(Attack());
    }

    public void StartAttacking()
    {
        _attacking = true;
        StartCoroutine(Attack());
    }

    public void LaunchBasicLaser()
    {
        LaserCollider laserbeam;

        laserbeam = Instantiate(_laserPrefab, _tf.position, _tf.rotation, _tf.parent);
        laserbeam.Init(gameObject, _spaceshipTarget, _basicDamage);
    }

    public void LaunchGuidedLaser()
    {
        LaserGuided laserbeamG;

        laserbeamG = Instantiate(_laserGuidedPrefab, _tf.position, _tf.rotation, _world);
        laserbeamG.Init(gameObject, _spaceshipTarget, _guidedDamage);
    }

    public IEnumerator Attack()
    {
        while (_attacking == true)
        {
            float triggerValue = _triggerAction.action.ReadValue<float>();

            // if (Input.GetKey(KeyCode.Space))
            if (triggerValue > 0)
            {
                if (_numberOfGuidedLaser > 0)
                {
                    LaunchGuidedLaser();
                    _numberOfGuidedLaser--;
                }
                else
                {
                    LaunchBasicLaser();
                }
                //Debug.Log(gameObject.name + " a fait feu !!");

                yield return new WaitForSeconds(_attackDelay);
            }

            yield return new WaitForSeconds(0);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        _attacking = false;

        _tf = GetComponent<Transform>();

        _target = gameObject;

        //StartCoroutine(Attack());
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void upgradeDamage()
    {
        _basicDamage *= 1.5f;
        _guidedDamage *= 1.5f;
    }
}
