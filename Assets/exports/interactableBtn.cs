using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class interactableBtn : MonoBehaviour
{
    private XRGrabInteractable xrGrabInteractable;

    private void Start()
    {
        xrGrabInteractable = GetComponent<XRGrabInteractable>();

        if (xrGrabInteractable == null)
        {
            Debug.LogError("Le composant XRGrabInteractable est manquant sur ce GameObject.");
            return;
        }

        // Abonnez-vous à l'événement de sélection pour le bouton
        xrGrabInteractable.onSelectEntered.AddListener(OnSelectionEntree);
    }

    private void OnSelectionEntree(XRBaseInteractor interactor)
    {
        // Méthode appelée lorsque le bouton est cliqué
        Debug.Log("Bouton cliqué !");
        // Ajoutez ici le code pour activer/désactiver les panels, etc.
    }
}
