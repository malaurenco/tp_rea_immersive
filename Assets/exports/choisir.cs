using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using TMPro;
using UnityEngine;
using UnityEngine.UI;



public class choisir : MonoBehaviour
{
    public TMP_Text texteTotalKiloOr;
    public TMP_Text texteTotalKiloOr2;
    public TMP_Text texteTotalKiloOr1;

    public GameObject _radar;
    public GameObject _radarButton;

    public GameObject _cannonRight;
    public GameObject _cannonLeft;

    public GameObject _viseAsteroide;

    private bool _isRadrBuy;

    private int totalKiloOr;
    private int solde;

    public GameObject _buyText;


    public GameObject _glodStorage;
    public GameObject _totalButton;

    public int _prixTeteChercheuse = 3;
    public int _prixPlusDegats = 7;
    public int _prixMieuxMinage = 10;
    public int _prixActiveRadar = 20;

    int[] _buyable;
    

    // Start is called before the first frame update
    void Start()
    {
        _buyable = new int[4];
        for(int i = 0; i < 4; i++)
        {
            _buyable[i] = 0;
        }
        _isRadrBuy = false;
    }

    // Update is called once per frame
    void Update()
    {

    }

    // M�thode appel�e lorsqu'un bouton d'achat est cliqu�
    public void OnBoutonAchatClique(int whatBuy)
    {
        _buyable[whatBuy]++;
        // Mettez � jour le total des kilos d'or
        switch (whatBuy)
        {
            case 0: totalKiloOr += _prixTeteChercheuse; break;
            case 1: totalKiloOr += _prixPlusDegats; break;
            case 2: totalKiloOr += _prixMieuxMinage; break;
            case 3: totalKiloOr += _prixActiveRadar; _radarButton.SetActive(false); break;
            default: break;

        }
        

        // Mettez � jour le texte affichant le total
        
        texteTotalKiloOr.text = "Total : " + totalKiloOr + " kg";

        if(_glodStorage.GetComponent<GoldGestion>().GetGold() >= totalKiloOr)
        {
            _totalButton.GetComponent<Image>().color = new Color(255, 255, 255);
        }
        else
        {
            _totalButton.GetComponent<Image>().color = new Color(255, 0, 0);
        }
    
        //solde = 100000 - totalKiloOr;
        
        //texteTotalKiloOr2.text = "solde : " +  solde + " kg";
        //texteTotalKiloOr1.text = "solde : " + solde + " kg";
    }

    public void onBuy()
    {
        _glodStorage.GetComponent<GoldGestion>().onBuy();
        if (_glodStorage.GetComponent<GoldGestion>().TryRemoveGold(totalKiloOr)){

             _cannonLeft.GetComponent<SpaceshipCanon>().AddGuidedLaser(_buyable[0]);
            _cannonRight.GetComponent<SpaceshipCanon>().AddGuidedLaser(_buyable[0]);

            for (int i = 0; i < _buyable[1]; i++)
            {
                _cannonLeft.GetComponent<SpaceshipCanon>().upgradeDamage();
                _cannonRight.GetComponent<SpaceshipCanon>().upgradeDamage();
            }
            for (int i = 0; i < _buyable[2]; i++)
            {
                _viseAsteroide.GetComponent<asteroideMining>().upgrade();
            }
            if (_buyable[3] != 0)
            {
                _isRadrBuy = true;
                _radar.SetActive(true);
            }
            _buyText.GetComponent<TextMeshProUGUI>().text = "Merci pour votre achat";
        }
        else
        {
            if (!_isRadrBuy)
            {
                _radarButton.SetActive(true);
            }
            _buyText.GetComponent<TextMeshProUGUI>().text = "Vous n'avez pas\nassez d'or !";
        }

        for (int i = 0; i < 4; i++)
        {
            _buyable[i] = 0;
        }

        totalKiloOr = 0;
        texteTotalKiloOr.text = "Total : " + totalKiloOr + " kg";
        
    }
}
