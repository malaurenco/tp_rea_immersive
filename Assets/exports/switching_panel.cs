using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
//using UnityEngine.SceneManager;

public class switching_panel : MonoBehaviour
{

    public GameObject menupanel;
    public GameObject infopanel;
    // Start is called before the first frame update
    void Start()
    {
        menupanel.SetActive(true);
        infopanel.SetActive(false);
    }

    // Update is called once per frame
    public void infoButton()
    {
        menupanel.SetActive(false);
        infopanel.SetActive(true);
    }
    public void backButton()
    {
        menupanel.SetActive(true);
        infopanel.SetActive(false);
    }
    public void Quit()
    {
        Application.Quit();
    }
    public void startBtn(string scenename)
    {
        SceneManager.LoadScene(scenename);
    }
}
